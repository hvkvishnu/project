package family.tree;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Element;
public class TestFamilyTree {
	Family family;
	Element davidFamilyElement;
	Element celineFamilyElement;
	FamilyTree familyTree;
	@Before
	public void initialize(){
		family = new Family();
		davidFamilyElement = (Element)family.getNodeList().item(0);
		celineFamilyElement = (Element) family.getNodeList().item(3);
		familyTree = new FamilyTree();
	}
	@Test
	public void testGetUser(){
		assertNull(family.getUser("sreelekha", "pothurai", "female"));
		assertSame(family.getUser("David", "Smith", "male"), davidFamilyElement);
	}
	@Test
	public void testFamilyName(){
		assertTrue(family.checkFamilyName(davidFamilyElement, "Smith"));
	}
	@Test
	public void testCheckSon(){
		assertTrue(family.checkSon(davidFamilyElement, "David"));
	}
	@Test
	public void testCheckDaughter(){
		assertTrue(family.checkDaughter(celineFamilyElement, "Celine"));
	}
	@Test
	public void testSisterBrother(){
		Element michel = family.getUser("Michel", "Smith", "male");
		Element mary = family.getUser("Mary", "Smith", "female");
		assertFalse(familyTree.checkMarriage(new Constraints(), michel, mary));
	}
}

package family.tree;

import org.w3c.dom.Element;

public class Constraints {
	public boolean sameFatherFamily(Element boy, Element girl){
		return boy.getAttribute("name").equals(girl.getAttribute("name"));
	}
	public boolean sameMotherFamily(Element boy,Element girl){
		return getMotherFamily(boy).equals(getMotherFamily(girl));
	}
	public boolean checkMotherFatherFamily(Element e1,Element e2){
		return getMotherFamily(e1).equals(e2.getAttribute("name"));
	}
	public String getMotherFamily(Element element){
		Element mother = (Element) element.getElementsByTagName("wife").item(0);
		return mother.getAttribute("maiden");
	}
}

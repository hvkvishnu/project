package family.tree;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class FamilyTree {
	public boolean checkMarriage(Constraints constraints,Element david,Element celine){
		if(!(constraints.sameFatherFamily(david, celine) && constraints.sameMotherFamily(david, celine))){
			if(constraints.checkMotherFatherFamily(david, celine) || constraints.checkMotherFatherFamily(celine, david)){
				System.out.println("They can marryyy.....They are some how related");
				return true;
			}
			else{
				System.out.println("They can marry....But not related");
				return true;
			}
		}
		else{
			System.out.println("They cannot marry....");
			return false;
		}
	}
	public static void main(String[] args) {
		Family family = new Family();
		Element david = family.getUser("David", "Smith", "male");
//		System.out.println(david.getAttribute("name"));
		Element celine = family.getUser("Celine", "Jones", "female");
//		System.out.println(celine.getAttribute("name"));
		
		FamilyTree ft = new FamilyTree();
		boolean marriage = ft.checkMarriage(new Constraints(), david, celine);
		if(marriage){
			System.out.println("Hurrahhhh.....Marrige....Marriage...");
		}
		else
			System.out.println("sad......they are not gonna married....");
		
	}
}

class Family{
	private static String fileName = "familytree.xml";
	private NodeList nodeList;
	public Family() {
		try{
			this.nodeList = getDocument().getElementsByTagName("family");
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	public NodeList getNodeList(){
		return nodeList;
	}
	public Document getDocument() throws Exception{
		File xmlFile = new File(fileName);
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder dbuilder = dbf.newDocumentBuilder();
		Document document = dbuilder.parse(xmlFile);
		document.getDocumentElement().normalize();
		return document;
	}
	
	public Element getUser(String name,String familyName,String gender){
		int length = this.nodeList.getLength();
		for(int i=0;i<length;i++){
			Element element = (Element) nodeList.item(i);
			if(checkFamilyName(element, familyName)){
				if(gender.toLowerCase() == "male" && checkSon(element,name)){
					return element;
				}
				else if(checkDaughter(element,name)){
					return element;
				}
			}
		}
		return null;
	}
	public boolean checkFamilyName(Element element, String familyName){
		return element.getAttribute("name").equals(familyName);
	}
	
	public boolean checkSon(Element element,String name){
		return getTagValue("son", element,name);
	}
	public boolean checkDaughter(Element element,String name){
		return getTagValue("daughter", element,name);
	}
	public Boolean getTagValue(String tag, Element element,String name) {
		NodeList nodeList = element.getElementsByTagName(tag);
//		System.out.println(nodeList);
		for(int i =0;i<nodeList.getLength();i++){
			Element e = (Element) nodeList.item(i);
			if(e.getAttribute("name").equals(name))
				return true;
		}
		return false;
	}
	
}

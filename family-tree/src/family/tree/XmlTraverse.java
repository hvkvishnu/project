package family.tree;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class XmlTraverse {
	public static void main(String[] args) throws Exception{
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setIgnoringElementContentWhitespace(true);
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.parse("familytree.xml");
		Element rootelement = doc.getDocumentElement();
		System.out.println("The Root Element is :"+rootelement.getNodeName());
		System.out.println("The Length....:"+rootelement.getChildNodes().getLength());
		
		
		
	}
}

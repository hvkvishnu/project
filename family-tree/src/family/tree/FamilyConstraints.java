package family.tree;

import org.junit.Test;
import org.w3c.dom.Element;

import family.tree.Family;
import family.tree.FamilyTree;

import static org.junit.Assert.*;

import org.junit.Before;
public class FamilyConstraints {
	private Element david;
	private Element celine;
	private Constraints cs;;
	@Before
	public void initialize(){
		Family family = new Family();
		david = family.getUser("David", "Smith", "male");
		celine = family.getUser("Celine", "Jones", "female");
		cs = new Constraints();
	}
	@Test
	public void testFatherFamily(){
		assertFalse(cs.sameFatherFamily(david, celine));
	}
	@Test
	public void testMotherFamily(){
		assertFalse(cs.sameMotherFamily(david, celine));
	}
	@Test
	public void testMotherFatherFamily(){
		assertTrue(cs.checkMotherFatherFamily(david, celine));
		assertFalse(cs.checkMotherFatherFamily(celine,david));
	}
	
}
